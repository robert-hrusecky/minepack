mod download;
mod installer;
mod modpack;

use download::*;
use modpack::Modpack;

use anyhow::Result;
use clap::Clap;
use serde_json::Value as JsonValue;
use std::env;
use std::fs::OpenOptions;
use std::fs::{self, File};
use std::io::BufReader;
use std::io::Write;
use std::io::{self, Seek, SeekFrom};
use std::path::{Path, PathBuf};

fn game_dir() -> Result<PathBuf> {
    let base = {
        #[cfg(target_os = "linux")]
        {
            env::var("HOME")?
        }
        #[cfg(target_os = "windows")]
        {
            env::var("APPDATA")?
        }
    };
    let path = Path::new(&base).join(".minecraft");
    Ok(path)
}

fn remove_profile(profiles: &mut JsonValue, name: impl AsRef<str>) -> Option<JsonValue> {
    profiles
        .get_mut("profiles")?
        .as_object_mut()?
        .remove(name.as_ref())
}

fn add_profile(profiles: &mut JsonValue, name: impl AsRef<str>, value: JsonValue) -> Option<()> {
    profiles
        .get_mut("profiles")?
        .as_object_mut()?
        .insert(name.as_ref().to_string(), value);
    Some(())
}

fn fix_launcher_profiles(
    path: impl AsRef<Path>,
    game_dir: impl AsRef<Path>,
    display_name: impl AsRef<str>,
    game_version: impl AsRef<str>,
) -> Result<()> {
    let mut file = OpenOptions::new().read(true).write(true).open(path)?;
    let mut profiles: JsonValue = serde_json::from_reader(&file)?;
    let mut profile = remove_profile(
        &mut profiles,
        "fabric-loader-".to_owned() + game_version.as_ref(),
    )
    .ok_or(anyhow::anyhow!("Unexpected JSON in launcher profile"))?;
    let profile_obj = profile
        .as_object_mut()
        .ok_or(anyhow::anyhow!("Profile is not a JSON object"))?;
    profile_obj.insert(
        "gameDir".to_string(),
        game_dir
            .as_ref()
            .to_str()
            .ok_or(anyhow::anyhow!("gameDir is invalid unicode"))?
            .into(),
    );
    profile_obj.insert("name".to_string(), display_name.as_ref().into());

    add_profile(&mut profiles, display_name, profile);

    // go back to the beginning of the file
    file.seek(SeekFrom::Start(0))?;

    serde_json::to_writer_pretty(&mut file, &profiles)?;

    // chop off any excess data
    let pos = file.stream_position()?;
    file.set_len(pos)?;

    Ok(())
}

fn setup_mods_dir(mods_dir: impl AsRef<Path>) -> Result<()> {
    let mods_dir = mods_dir.as_ref();
    if mods_dir.exists() {
        print!(
            "This mod pack is already installed.\n\
             Do you want to delete the current \"mods\" folder before reinstalling? [Y/n]: "
        );
        io::stdout().flush()?;
        let mut answer = String::new();
        io::stdin().read_line(&mut answer)?;
        if answer
            .trim()
            .chars()
            .next()
            .map_or(false, |c| !c.eq_ignore_ascii_case(&'y'))
        {
            return Ok(());
        }
        println!("Deleting the mods folder.");
        fs::remove_dir_all(&mods_dir)?;
    }
    fs::create_dir_all(&mods_dir)?;
    Ok(())
}

#[derive(Clap)]
struct Opts {
    #[clap(name = "MODPACK", parse(from_os_str))]
    mockpack_path: PathBuf,
}

fn main_result() -> Result<()> {
    let args = Opts::parse();
    let spec = File::open(args.mockpack_path)?;

    // let pack: Modpack = serde_json::from_str(data)?;
    let pack: Modpack = serde_json::from_reader(BufReader::new(spec))?;

    // Setup directories
    let game_dir = game_dir()?;
    let launcher_profiles_path = game_dir.join("launcher_profiles.json");
    let modpack_dir = game_dir.join("modpacks").join(pack.name);
    let installer_path = modpack_dir.join("fabric-installer.jar");
    let mods_dir = modpack_dir.join("mods");

    setup_mods_dir(&mods_dir)?;

    download_as_path(
        "https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.10.2/fabric-installer-0.10.2.jar",
        &installer_path,
    )?;

    let version = pack
        .installer
        .version()
        .ok_or_else(|| anyhow::anyhow!("Forge is not supported yet"))?;

    installer::install_fabric(&installer_path, version, &pack.mc_version)?;
    fs::remove_file(installer_path)?;

    fix_launcher_profiles(&launcher_profiles_path, &modpack_dir, &pack.display_name, &pack.mc_version)?;

    let mut mods = Vec::new();

    let mut answer = String::new();
    for group in &pack.mods {
        if let Some(query) = &group.query {
            print!("{} [Y/n]: ", query);
            io::stdout().flush()?;

            answer.clear();
            io::stdin().read_line(&mut answer)?;
            if answer
                .trim()
                .chars()
                .next()
                .map_or(false, |c| !c.eq_ignore_ascii_case(&'y'))
            {
                continue;
            }
        }
        mods.extend(group.urls.iter());
    }

    download_mods(mods.into_iter(), &mods_dir)?;

    println!(
        "\nThe mod pack was installed successfully!\n\
         If you have the Minecraft launcher open, please restart it."
    );


    Ok(())
}

fn main() -> Result<()> {
    if let Err(e) = main_result() {
        eprintln!("{}", e);
    }

    // On windows, the user will run by double-clicking the modpack file. Keep the command window
    // open.
    #[cfg(target_os = "windows")]
    {
        println!("Press enter when you are finished reading.");
        io::stdout().flush()?;
        io::stdin().read_line(&mut String::new())?;
    }
    Ok(())
}
