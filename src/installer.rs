use std::path::Path;

use anyhow::Result;
#[cfg(target_os = "windows")]
use std::fs;
#[cfg(target_os = "windows")]
use std::path::PathBuf;
use std::process::Command;

#[cfg(target_os = "windows")]
fn windows_find_java_in(path: impl AsRef<Path>) -> Result<Option<PathBuf>> {
    for entry in fs::read_dir(path)? {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            if let Some(path) = windows_find_java_in(path)? {
                return Ok(Some(path));
            }
        } else {
            if path.ends_with("java.exe") {
                return Ok(Some(path));
            }
        }
    }
    return Ok(None);
}

pub fn install_fabric(
    jar: impl AsRef<Path>,
    version: impl AsRef<str>,
    mc_version: impl AsRef<str>,
) -> Result<()> {
    let java_path;
    #[cfg(target_os = "windows")]
    {
        // Use the java vendored with the minecraft launcher
        java_path = windows_find_java_in("C:\\Program Files (x86)/Minecraft Launcher/runtime")?
            .ok_or(anyhow::anyhow!("Could not find java"))?;
    }
    #[cfg(target_os = "linux")]
    {
        java_path = "java";
    }
    let status = Command::new(java_path)
        .arg("-jar")
        .arg(jar.as_ref().as_os_str())
        .arg("client")
        .arg("-mcversion")
        .arg(mc_version.as_ref())
        .arg("-loader")
        .arg(version.as_ref())
        .status()?;

    if !status.success() {
        anyhow::bail!("Installer returned bad exit status {}", status);
    }

    Ok(())
}
