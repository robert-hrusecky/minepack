use anyhow::Result;
use async_compat::CompatExt;
use futures::{future, StreamExt, TryFutureExt, TryStreamExt};
use hyperx::header::{ContentDisposition, DispositionParam, Header};
use indicatif::{MultiProgress, ProgressBar, ProgressFinish, ProgressStyle};
use percent_encoding::percent_decode;
use reqwest::IntoUrl;
use tokio::runtime::Runtime;
use std::io;
use std::path::Path;
use tokio::fs::File;
use tokio::io as tio;

fn create_response_bar(resp: &reqwest::Response) -> ProgressBar {
    resp.content_length()
        .map_or_else(ProgressBar::new_spinner, ProgressBar::new)
        .with_style(
            ProgressStyle::default_bar()
                .template("{msg}: {wide_bar} {bytes:5}/{total_bytes:5}")
                .on_finish(ProgressFinish::AndLeave),
        )
}

async fn download_as_remote(
    url: impl IntoUrl,
    dst: impl AsRef<Path>,
    bars: &MultiProgress,
) -> Result<()> {
    let url = url.into_url()?;
    let resp = reqwest::get(url.clone()).await?;
    let resp = resp.error_for_status()?;

    let file_name = resp
        .headers()
        .get(reqwest::header::CONTENT_DISPOSITION)
        .and_then(|header| ContentDisposition::parse_header(&header).ok())
        .and_then(|header| {
            header.parameters.iter().find_map(|param| {
                if let DispositionParam::Filename(_, _, bytes) = param {
                    let string = String::from_utf8_lossy(bytes);
                    let start = string.rfind('/').unwrap_or(0) + '/'.len_utf8();
                    Some(string[start..].to_string())
                } else {
                    None
                }
            })
        })
        .or_else(|| {
            resp.url()
                .path_segments()
                .or_else(|| url.path_segments())
                .and_then(Iterator::last)
                .map(|s| percent_decode(s.as_bytes()).decode_utf8_lossy())
                .map(Into::into)
        })
        .ok_or_else(|| anyhow::anyhow!("Unknown remote file name"))?;

    let path = dst.as_ref().join(file_name.to_string());

    let bar = create_response_bar(&resp).with_message(file_name.to_string());
    bars.add(bar.clone());

    let file = File::create(path).await?;
    download(resp, file, &bar).await
}

async fn download(resp: reqwest::Response, mut file: File, bar: &ProgressBar) -> Result<()> {
    let mut data = resp
        .bytes_stream()
        .map(|r| match r {
            Ok(chunk) => {
                bar.inc(chunk.len() as u64);
                Ok(chunk)
            }
            Err(e) => Err(io::Error::new(io::ErrorKind::Other, e)),
        })
        .into_async_read()
        .compat();
    tio::copy(&mut data, &mut file).await?;
    Ok(())
}

pub fn download_as_path(url: impl IntoUrl, path: impl AsRef<Path>) -> Result<()> {
    Runtime::new()?.block_on(async {
        let file = File::create(path).err_into::<anyhow::Error>();
        let resp = reqwest::get(url).err_into::<anyhow::Error>();
        let (file, resp) = tokio::try_join!(file, resp)?;
        let resp = resp.error_for_status()?;
        let bar = create_response_bar(&resp).with_message("downloading");
        download(resp, file, &bar).await
    })
}


pub fn download_mods(
    urls: impl Iterator<Item = impl IntoUrl>,
    mods_dir: impl AsRef<Path>,
) -> Result<()> {
    let mods_dir = mods_dir.as_ref();
    let bars = MultiProgress::new();
    Runtime::new()?.block_on(async {
        future::try_join_all(urls.map(|url| download_as_remote(url, mods_dir, &bars))).await
    })?;
    Ok(())
}
