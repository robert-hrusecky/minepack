use serde::Deserialize;

#[derive(Deserialize)]
#[serde(tag = "kind")]
pub enum Installer {
    Fabric { version: String },
    Forge,
}

impl Installer {
    pub fn version(&self) -> Option<&str> {
        match self {
            Self::Fabric { version } => Some(version),
            Self::Forge => None,
        }
    }
}

#[derive(Deserialize)]
pub struct ModGroup {
    pub urls: Vec<String>,
    pub query: Option<String>,
}

#[derive(Deserialize)]
pub struct Modpack {
    pub name: String,
    pub display_name: String,
    pub mc_version: String,
    pub installer: Installer,
    pub mods: Vec<ModGroup>,
}
